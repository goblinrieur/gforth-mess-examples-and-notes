#!/bin/bash

# to be run by ci/cd schedules only 
# Define retention policy (e.g., retain artifacts for the last 5 builds)
RETENTION_POLICY=5

# Get the total number of artifacts
TOTAL_ARTIFACTS=$(ls -1 artifacts | wc -l)

# Calculate the number of artifacts to be cleaned
ARTIFACTS_TO_BE_CLEANED=$((TOTAL_ARTIFACTS - RETENTION_POLICY))

# Clean artifacts
if [ $ARTIFACTS_TO_BE_CLEANED -gt 0 ]; then
  ls -t artifacts | tail -$ARTIFACTS_TO_BE_CLEANED | xargs rm
fi
