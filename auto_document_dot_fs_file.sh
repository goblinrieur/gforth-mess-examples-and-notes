#! /usr/bin/env bash
# -*- coding: UTF8 -*-
filelist=(*/*.fs)
displayedcount=0

# displays content words & commentary lines 
function describe() {
	echo -e "\e[32;1m"
	grep -iE "^:|\\\ |\ \(.*--.*\)" "$1"
	read
}

# generates menu & reads selection
function main() {
	echo -e "\e[33m See structure of : "
	for i in ${filelist[@]} ; do
		let "displayedcount+=1"
		echo -e "\e[32m\t- $displayedcount -\t$i" | sed 's/\.fs$//'
	done
	echo -e "which one ? \e[0m"
	read reply		 		# alignement of reply to filelist array
	let "reply-=1"
	describe  ${filelist[@]:$reply:1}	#rgexp filter ${filelist[@]:$reply:1}
}

# mode selection menu / one file
clear
if [[ "$1" == *".fs" ]] ; then
	describe "$1"
else
	main
fi
exit 0
