#! /usr/bin/env bash
fsfile=mandelbrot.fs
runner=$(which gforth-fast)
[ $(grep gforth-fast <<< ${runner}) ] && $runner $fsfile "$@" || ( echo "gforth not found" && exit 1 )
exit 0

