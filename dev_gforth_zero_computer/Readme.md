see [this site for reference](http://edmundgriffiths.com/czero.html)

see [this site for tutorial](http://edmundgriffiths.com/degreezero.html)

see [this PDF if site is no more available](./Computer_0.pdf)

output example might be 
```
Prisoner  run-sample-program 
  0  0 : NOP  0 ->   0  1
  0  1 : NOP  0 ->   0  2
  0  2 : STP  0 ->   0 -1
```

