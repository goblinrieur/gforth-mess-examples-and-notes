initial example is taken from [HERE](https://rosettacode.org/wiki/Chaos_game#Forth)

currently working with gforth 0.7.3 for example

```
./chaos.fs 1000 1000000 chao-game.pbm
width:  1000 
steps:  1000000 
output: chao-game.pbm
```

```
chao-game.pbm
chaos.fs
```

![chao-game.pbm](./chao-game.pbm)

![chao-game.png](./chao-game.png)

