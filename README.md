just a mess of some forth examples of code 

look at sub-directories 

some fun stuff 

```
DECIMAL
27 CONSTANT ESC 
: COLORIZE ESC EMIT ." [" base @ >R 0 <# #S #> type R> base ! ." m"  ; \ ASCII TERMINAL ONLY 

: checkargs 
	argc @ 4 < if 
                31 COLORIZE ( just like a \033[31m )
                cr cr ." error need 4 args on command line " cr cr 
                0 COLORIZE
		bye
	then
; 
( ... some stuff ... ) 
checkargs 
main
bye
( ... some stuff ... ) 
```

Get in mind all this is a pool of example, nothing is really ended neither guaranteed to work properly.

I'm currently using gforth 0.7.3 for testing

![FORTH](./ceccefe0-138d-46b6-8d05-bc5609c9a915.png)

etc...  

[15squares solver](./mess_example/15squaressolver.fs)

[bull & cows game](./mess_example/bulls_and_cows.fs)

[C_function](./mess_example/C_funct_gforth.fs)

[computer_zero](./dev_gforth_zero_computer)

[cyphercaesar](./mess_example/cyphercaesar.fs)

[brainf](./mess_example/bf.fs)

[chaos](./dev_gforth_chaospyramide)

[cement](./dev_gforth_ciment)

[dice](./dev_gforth_dice)

[draw ascii sphere](./mess_example/asciisphere.fs)

[get_day_from_date](./mess_example/get_day_from_date.fs)

[Hanoi](./mess_example/hanoi.fs)

[are there solutions to a+B+C=A.B.C ?](./mess_example/isthere_a+b+c_equ_abc.fs)

[kmh to kms](./mess_example/kmh_to_ms.fs)

[kms_to_knots](./mess_example/kmh_to_knots.fs)

[Mandelbrot](./dev_gforth_mandelbrot)

[maze](./dev_gforth_maze)

[Nth-root](./mess_example/Nth-root.fs)

[Pythagoras](./mess_example/pythagore.fs)

[roman-Arabic-converter](./mess_example/roman-arabic-converter.fs)

[scriptin](./dev_gforth_scriptin/scriptin.fs)

[syracuse (hail)](./mess_example/syracuse.fs)

[temp](./mess_example/temp.fs)

[test-zero](./mess_example/testzero.fs)

[text align](./mess_example/align_file_toto.txt_coulmns.fs)

etc...
