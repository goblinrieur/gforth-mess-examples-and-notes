# 21 sticks game

This is just a one-hour project, might be bugged at first.

Goal here is to force computer to keep last (here stars Unicode displayed) tile/sticks. 

In this version we plays on red tiles. You & computer can remove 1 to 4 tiles at once, no more no less each turn.

# How does it look 

![start](./pics/start.png)

![2](./pics/2.png)

![3](./pics/3.png)

![win](./pics/wining.png)

# Auto documentation 

[doc](./21sticksgame.fs.doc)

# code

[code](./21sticksgame.fs)
